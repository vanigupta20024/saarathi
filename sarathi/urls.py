from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from home import views
from elder import views as views_e

urlpatterns = [
    path('admin/', admin.site.urls),
    url('^$', views.register, name='register'),
    url('register', views.register),
    path('login/', views.loginPage, name="login"),  
    path('logout/', views.logoutPage, name="logout"),
    path('elder/home', views_e.home, name="elder_home"),
    path('elder/home_s', views_e.sos_emergency, name="sos_emergency"),
    path('elder/hospital', views_e.hospital, name="elder_hospital"),
    path('elder/housekeeping', views_e.housekeeping, name="elder_housekeeping"),
    path('elder/walksupport', views_e.walksupport, name="elder_walksupport"),
    path('elder/mentalsupport', views_e.mentalsupport, name="elder_mentalsupport"),
    path('elder/healthsupport', views_e.healthsupport, name="elder_healthsupport"),

]
