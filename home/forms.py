from django.contrib.auth import get_user_model
from django import forms

user = get_user_model()

class RegistrationForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password1 = forms.CharField(
        label = 'Password',
        widget = forms.PasswordInput
    )
    password2 = forms.CharField(
        label = 'Confirm Password',
        widget = forms.PasswordInput
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = user.objects.filter(username__iexact=username)
        if qs.exists():
            raise forms.ValidationError("This is an invalid user, Pick another!")
        return username

    def clean_email(self):
        email = self.cleaned_data.get("email")
        qs = user.objects.filter(email__iexact=email)
        if qs.exists():
            raise forms.ValidationError("Email is already been taken choose another!")
        return email

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(
        widget = forms.PasswordInput
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = user.objects.filter(username__iexact=username)
        if not qs.exists():
            raise forms.ValidationError("This is an invalid user.")
        return username