from django.shortcuts import render, redirect 
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, RegistrationForm

User = get_user_model()

def register(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password1 = form.cleaned_data.get("password1")
        password2 = form.cleaned_data.get("password2")
        user = User.objects.create_user(username, email, password1)
        if user != None:
            login(request, user)
            return redirect("login")
        else: 
            request.session['registration_error'] = 1
    return render(request, 'home/register.html', {"form":form})

def loginPage(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username = username, password = password)
        if user != None:
            login(request, user)
            return redirect("/elder/home")
        else:
            request.session['invalid_user'] = 1
    return render(request, 'home/login.html', {"form":form})

def logoutPage(request):
	logout(request)
	return redirect('login')