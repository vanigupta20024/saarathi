from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings # new
from django.http.response import JsonResponse
import requests
from twilio.rest import Client


def home(request):
    return render(request, 'elder/home.html', {'key':settings.STRIPE_PUBLISHABLE_KEY})

def housekeeping(request):
    return render(request, 'elder/housekeeping.html')

def walksupport(request):
    return render(request, 'elder/walksupport.html')

def mentalsupport(request):
    return render(request, 'elder/mentalsupport.html')

def healthsupport(request):
    return render(request, 'elder/healthsupport.html')

def hospital(request):
    hospital_dic = {}
    public_ip = requests.get('https://api.ipify.org').text
    data = requests.get(f"http://api.ipstack.com/{public_ip}?access_key=4e1b7877567c438b18618554bab2311b").json()
    lat, lon = data['latitude'], data['longitude']
    hospital = requests.get(f'https://places.ls.hereapi.com/places/v1/autosuggest?at={lat},{lon}&q=hospital&size=5&apiKey=Po9P5iMpnETlXYMShv7paQpPfbs6m7GPO0yK5dU2qbI').json()
    for item in hospital['results']:
        hospital_dic[item['title']] = 'Delhi'
    return render(request, 'elder/hospital.html', {'hospital':hospital_dic})

def sos_emergency(request):
    account_sid = 'ACde270ebe8ab8cb1a86b8ef7c2b29987c'
    auth_token = '04bb0837057453844a97d0dcd283f4e0'
    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                         body="Join Earth's mightiest heroes. Like Kevin Bacon.",
                         from_='+12764517699',
                         to='+919582897457'
                     )
    print(message.sid)
    return render(request, 'elder/home.html', {"toast":'True', 'key':settings.STRIPE_PUBLISHABLE_KEY})
    