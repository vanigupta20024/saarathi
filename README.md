# Sarathi

Everyone has been hit by lockdown and Covid-19 but then these people, either senior citizens or disabled people have been hit the hardest since they could not go out, ask for help etc. since most of the services shut down abruptly, leaving no choice for them and making them helpless. We thought why not develop something to help these people, which in turn could make a huge impact on their day to day life! We not only added basic functionalities but also added USPs like donation drives(a study said that 85% disabled people borrowed money during lockdown) and SOS feature to alert people which can save a life!

It connects and pairs volunteers who want to assist with people who need help like people who are over a specific age or need special assistance. The volunteers can help them with walk support, helping them with deliverables or assisting in basic household chores. Then we have added payment gateway for donation and SOS feature. We are also listing nearest hospitals based on the IP address of the user. The users can book appointments with doctors and can call out for mental support and counseling as lockdown has taken a toll on people's mental health.

# How to run ?

1. Install requirements:

`pip install -r requirements.txt`

2. Change the directory to sarathi-master:

`cd sarathi-master`

3. To sync models with database:

`python manage.py makemigrations`

`python manage.py migrate`

4. Finally run the server:

`python manage.py runserver`

5. Copy the server IP and paste in the browser (in this case `http://127.0.0.1:8000/`)

You can either login with root user i.e.
```
username : sarathi
password : password
```

Or you can register as a new user.

# Demo Video Link
[![Sarathi impact hackathon submission](/overview.png)](https://youtu.be/_GLuJu5ekJc)
